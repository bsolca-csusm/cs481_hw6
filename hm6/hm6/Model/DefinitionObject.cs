﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hm6.Model
{
    public class DefinitionData
    {
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Example { get; set; }
        public string ImageUrl { get; set; }
        public string Emoji { get; set; }
    }

    public class DefinitionObject
    {
        public List<DefinitionData> Definitions { get; set; }
        public string Word { get; set; }
        public string Pronunciation { get; set; }
    }
}
