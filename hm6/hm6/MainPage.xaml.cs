﻿using hm6.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace hm6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly string _OwlBotApiToken = "9ff929b77acd6f52681f7121091ca10f50aa7d8f";
        private readonly string _getUrl = "https://owlbot.info/api/v4/dictionary/";
        private readonly HttpClient _client;

        public MainPage()
        {
            InitializeComponent();
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", _OwlBotApiToken);
            MyProgressBar.IsRunning = false;
        }

        private async Task GetDefinition()
        {
            MyProgressBar.IsRunning = true;
            if (!CrossConnectivity.Current.IsConnected)
            {
                MyWord.Text = "No internet connexion";
                MyStack.Children.Clear();
                MyProgressBar.IsRunning = false;
                return;
            }
            HttpResponseMessage response = await _client.GetAsync(_getUrl + MyEntry.Text);
            var res = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine("[INFO] API response: " + res);
            try
            {
                DefinitionObject objTest = JsonConvert.DeserializeObject<DefinitionObject>(res);
                MyWord.Text = objTest.Word;
                SetDefinitionTextFields(objTest);
            } catch (JsonSerializationException)
            {
                MyWord.Text = "No definition was found";
                MyStack.Children.Clear();
            }
            
            MyProgressBar.IsRunning = false;
        }

        private void SetDefinitionTextFields(DefinitionObject obj)
        {
            if (obj == null || obj.Word == null)
            {
                MyStack.Children.Clear();
                return;
            }
            MyStack.Children.Clear();
            MyWord.Text = obj.Word;
            Pronunciation.Text = obj.Pronunciation != null ? "Pronunciation: " + obj.Pronunciation : "No pronunciation available";
            obj.Definitions.ForEach(elem =>
            {
                Label type = new Label();
                Label definition = new Label();
                Label example = new Label();
                type.BackgroundColor = Color.LightSeaGreen;
                type.Text = elem.Type != null ? "Type: " + elem.Type : "No type available";
                MyStack.Children.Add(type);
                definition.Text = elem.Definition != null ? "Definition: " + elem.Definition : "No definition available";
                MyStack.Children.Add(definition);
                if (elem.Example != null)
                {
                    example.Text = "Example: " + elem.Example;
                    MyStack.Children.Add(example);
                }
            });
        }

        private async void Entry_Completed(object sender, EventArgs e)
        {
            await GetDefinition();
        }
    }
}
